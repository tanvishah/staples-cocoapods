Pod::Spec.new do |s|
  s.name         = "StaplesAdvantageChat"
  s.version      = "0.0.5"
  s.summary      = "This pod has customized version of atlas"
  s.homepage     = "https://bitbucket.org/tanvishah/staplesadvantagechat/src"
  s.license      = "MIT"
  s.author       = { "Jayashree Varpe" => "jayashree.varpe@mindstix.com"}
  s.source       = { :git => "https://bitbucket.org/tanvishah/staplesadvantagechat.git", :tag => s.version }
  s.source_files = 'staplesadvantagechat/*.*',
				  'staplesadvantagechat/**/*.*'
				  'staplesadvantagechat/**/**/*.*'
  s.requires_arc = true
  s.resource_bundles = {
    'StaplesAdvantageChat' => ['staplesadvantagechat/Resources/*.png','staplesadvantagechat/Resources/xibs/.xib']
  }

  s.dependency "LayerKit", "0.19.1"
  s.ios.deployment_target = '8.0'

end
